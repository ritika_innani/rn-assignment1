import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, TextInput, ScrollView, View, Button, Text, FlatList } from 'react-native';
import TaskItem from "./components/TaskItem";
import TaskInput from "./components/TaskInput";

export default function App() {
  
  const [tasksList, setTasks] = useState([]);
  const [isAddMode, setAddMode] = useState(false);

  const addTaskHandler = task => {
    setTasks(currentTasks => [
      ...currentTasks,
      { id: Math.random().toString(), value: task }
    ]);
    setAddMode(false);
  }

  const removeTaskHandler = id => {
    setTasks(currentTasks => {
      return currentTasks.filter((item) => {
        return item.id !== id;
      })
    })
  }

  const cancelTaskAdd = () => {
    setAddMode(false);
  }

  return (
    <View style={styles.screen}>
      <Button title="Add new task" onPress={() => setAddMode(true)} />
      <TaskInput visible={isAddMode} onAddTask={addTaskHandler} onCancel={cancelTaskAdd} />
      <View>
        {/* <ScrollView></ScrollView> */}
        <FlatList
          keyExtractor={(item, index) => item.id}
          data={tasksList}
          renderItem={itemdata => (
            <TaskItem id={itemdata.item.id} 
              onDelete={removeTaskHandler} 
              title={itemdata.item.value} 
            />
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 50
  },
  

});
