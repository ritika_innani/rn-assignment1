import React, { useState } from "react";
import { View, TextInput, Button, StyleSheet, Modal } from "react-native";

const TaskInput = props => {
    const [newTask, addTask] = useState('');

    const taskInputHandler = (task) => {
        addTask(task);
    }

    const addTaskHandler = () => {
        props.onAddTask(newTask);
        addTask('');
    }

    return (
        <Modal visible={props.visible} animationType="slide">
            <View style={styles.inputContainer}>
                <TextInput placeholder="Add your task" style={styles.input}
                    onChangeText={taskInputHandler}
                    value={newTask}
                />
                <View style={styles.buttonContainer}>
                    <View style={styles.button}>
                        <Button  title="ADD" onPress={addTaskHandler} />
                    </View>
                    <View style={styles.button} >
                        <Button title="CANCEL" color="gray" onPress={props.onCancel} />
                    </View>
                </View>
                {/* <Button style={styles.button} title="ADD" onPress={props.onAddTask.bind(this, newTask)} /> */}
            </View>
        </Modal>
    )
}

export default TaskInput;

const styles = StyleSheet.create({
    modal: {
        padding: 50
    },
    inputContainer: {
        justifyContent: 'center', alignItems: 'center', flex: 1
    },
    input: {
        borderColor: 'black', borderWidth: 1, padding: 10, width: '80%'
    },
    button: {
        width: '40%'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '60%',
        marginTop: 30
    }
})