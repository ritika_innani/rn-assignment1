import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, TouchableHighlight, TouchableNativeFeedback, TouchableWithoutFeedback } from "react-native";

// More touchable components are - TouchableHighlight, TouchableNativeFeedback, TouchableWithoutFeedback

const TaskItem = props => {
    // we can use props.children for passing data in between the opening and closing tag of this element
    return (
        <TouchableOpacity activeOpacity={0.8} onPress={() => props.onDelete(props.id)}>
            <View style={styles.listStyle} >
                <Text>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default TaskItem;

const styles = StyleSheet.create({
    listStyle: {
        padding: 10,
        backgroundColor: '#ccc',
        borderColor: 'black',
        borderWidth: 1,
        marginTop: 10,
        borderRadius: 10
    }
});